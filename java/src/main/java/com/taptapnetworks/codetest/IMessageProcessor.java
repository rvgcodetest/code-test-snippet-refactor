package com.taptapnetworks.codetest;

public interface IMessageProcessor {
  
  public static final String SMS = "SMS";
  public static final String MAIL = "mail";
  
  /**
   * Sets message with all params
   * @param msg message to be send
   */
  public void setupMessage(Message msg);

  /**
   * Sends message
   */
  public void send();
}
