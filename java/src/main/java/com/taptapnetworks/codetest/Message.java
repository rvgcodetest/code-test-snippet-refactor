package com.taptapnetworks.codetest;

/**
 * Information about message (to, message and subject)
 * @author rvelasco
 *
 */
public class Message {
  private String to = null;
  private String message = null;
  private String subject = null;
  
  public Message(String to, String message, String subject){
    this.to = to;
    this.message = message;
    this.subject = subject;
  }
  
  public String getTo() {
    return to;
  }
  
  public void setTo(String to) {
    this.to = to;
  }
  
  public String getMessage() {
    return message;
  }
  
  public void setMessage(String message) {
    this.message = message;
  }
  
  public String getSubject() {
    return subject;
  }
  
  public void setSubject(String subject) {
    this.subject = subject;
  }

}
