package com.taptapnetworks.codetest;

public class SMSProcessor extends MessageProcessor {

  private Message msg = null;

  /**
   * Sets message to be processed
   * @param msg message to send
   */
  @Override
  public void setupMessage(Message msg) {
    this.msg = msg;
  }

  @Override
  public void send() {
    System.out.println(String.format("Sent message to %s", this.msg.getTo()));
    System.out.println(this.msg.getMessage());
  }
}
