package com.taptapnetworks.codetest;

public class SnippetToRefactor {
  private static String SMS_HOST = "192.168.1.4";
  private static String SMS_PORT = "4301";
  private static String SMS_USERNAME = "your-username";
  private static String SMS_PASSW = "your-password";
  private static String MAIL_HOST = "mail.myserver.com";
  
  private MessageProcessor msgProc = null;
  private IMessageConnection conn = null;

  public void send(String type, String message, String to, String subject) {
    
    if (MessageProcessor.isSms(type)) {
      // Processing SMS message
      msgProc = new SMSProcessor();
      conn = new SmsConnection(SMS_HOST, SMS_PORT);
      conn.setCredentials(SMS_USERNAME, SMS_PASSW);
    } else if (MessageProcessor.isMail(type)) {
      // Processing Mail message
      msgProc = new MailProcessor();
      conn = new MailConnection(MAIL_HOST);
    } else {
      System.out.println("Type "+type+" is unknown and won't be processed.");
      throw new RuntimeException("Type is unknown");
    }

    // Establish connection and send message
    msgProc.setConnection(conn.openConnection());
    msgProc.setupMessage(new Message(to, message, subject));
    msgProc.send();
    conn.close();
  }
}
