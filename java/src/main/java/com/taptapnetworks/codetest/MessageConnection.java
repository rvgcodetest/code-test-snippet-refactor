package com.taptapnetworks.codetest;

public abstract class MessageConnection implements IMessageConnection {
  
  /**
   * Hides password
   * @param password
   * @return hidden password
   */
  protected static String hidePassword(String password) {
    StringBuilder sb = new StringBuilder();
    for (int i = 0; i < password.length(); i++) {
      sb.append("*");
    }
    return sb.toString();
  }

  /**
   * Sets credentials to establish authenticate
   */
  @Override
  public abstract void setCredentials(String username, String passw);

  /**
   * Close current connection
   */
  @Override
  public abstract void close();
}
