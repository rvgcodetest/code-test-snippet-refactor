package com.taptapnetworks.codetest;

public class SmsConnection extends MessageConnection {

  private String host = null;
  private String port = null;
  private String userName = null;
  private String password = null;


  public SmsConnection(String host, String port) {
      this.host = host;
      this.port = port;
  }
  
  @Override
  public void setCredentials(String userName, String password) {
    this.userName = userName;
    this.password = password;
  }
  
  @Override
  public void close() {
      System.out.println("Connection closed");
  }

  @Override
  public IMessageConnection openConnection() {
    String hiddenPassword = hidePassword(this.password);
    System.out.println(String.format("Logged %s with password %s to %s:%s", this.userName, hiddenPassword, host, port));
    return this;
  }
}
