package com.taptapnetworks.codetest;

public class MailConnection extends MessageConnection{

  public MailConnection(String mailServer) {
    super();
    System.out.println(String.format("Created SendMailConnection for server %s", mailServer));
  }

  @Override
  public void setCredentials(String username, String passw) {
  }

  @Override
  public void close() {
    System.out.println("Mail connection closed");
  }

  @Override
  public IMessageConnection openConnection() {
    return this;
  }
}
