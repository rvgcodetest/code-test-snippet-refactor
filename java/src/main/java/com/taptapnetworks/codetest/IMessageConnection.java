package com.taptapnetworks.codetest;

public interface IMessageConnection {
  
  /**
   * Sets credentials for current connection
   * @param username
   * @param passw
   */
  public void setCredentials(String username, String passw);
  
  /**
   * Opens connection
   */
  public IMessageConnection openConnection();
  
  /**
   * Close established connection
   */
  public void close();
}
