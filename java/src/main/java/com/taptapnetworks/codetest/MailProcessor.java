package com.taptapnetworks.codetest;

public class MailProcessor extends MessageProcessor{

  @Override
  public void setupMessage(Message msg) {
    System.out.println(String.format("Preparing message for %s", msg.getTo()));
    System.out.println(String.format("Subject: %s", msg.getSubject()));
    System.out.println(msg.getMessage());
  }

  @Override
  public void send() {
    System.out.println("Message sent!!!");
  }
}
