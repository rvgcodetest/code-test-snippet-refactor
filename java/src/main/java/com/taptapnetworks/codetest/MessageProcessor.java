package com.taptapnetworks.codetest;

public abstract class MessageProcessor implements IMessageProcessor {

  private IMessageConnection conn = null;
  
  public static boolean isSms(String type) {
    return IMessageProcessor.SMS.equals(type);
  }
  
  public static boolean isMail(String type) {
    return IMessageProcessor.MAIL.equals(type);
  }
  
  public void setConnection(IMessageConnection conn){
    this.conn = conn;   
  }
  
  public abstract void setupMessage(Message msg);

  public abstract void send();

}
