package com.taptapnetworks.codetest;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class MessageProcessorTest {

  @Before
  public void setUp() throws Exception {
  }

  @Test
  public void testIsSms() {
    assertTrue(MessageProcessor.isSms(IMessageProcessor.SMS));
    assertFalse(MessageProcessor.isSms("OtherType"));
  }

  @Test
  public void testIsMail() {
    assertTrue(MessageProcessor.isMail(IMessageProcessor.MAIL));
    assertFalse(MessageProcessor.isMail("OtherType"));
  }

}
