package com.taptapnetworks.codetest;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class MailProcessorTest {
  MailProcessor proc = null;

  @Before
  public void setUp() throws Exception {
    proc = new MailProcessor();
  }

  @Test
  public void testSetupMessage() {
    proc.setupMessage(new Message("anyone", "test message", "test subject"));
  }

  @Test
  public void testSend() {
    proc.setupMessage(new Message("anyone", "test message", "test subject"));
    proc.send();
    assertTrue(true);
  }
}
