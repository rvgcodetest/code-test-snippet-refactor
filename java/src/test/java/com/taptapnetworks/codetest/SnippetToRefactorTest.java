package com.taptapnetworks.codetest;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

/**
 * @author rvelasco
 *
 */
public class SnippetToRefactorTest {

  private SnippetToRefactor objTest = null;
  
  @Before
  public void setUp() throws Exception {
    objTest = new SnippetToRefactor();
  }
  
  @Test
  public void testSMS() {
    objTest.send(IMessageProcessor.SMS, "Test of SMS message", "Carls", "SMS test");
    // At this point message has been processed and sent
    assertTrue(true);
  }
  
  @Test
  public void testMail() {
    objTest.send(IMessageProcessor.MAIL, "Test of Mail message", "Berg", "Mail test");
    // At this point message has been processed and sent
    assertTrue(true);
  }
}
