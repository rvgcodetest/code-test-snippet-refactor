package com.taptapnetworks.codetest;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class SmsConnectionTest {

  private SmsConnection conn = null;
  
  @Before
  public void setUp() throws Exception {
    conn = new SmsConnection("localhost", "8080");
  }

  @Test
  public void testOpenConnection() {
    conn.setCredentials("pepe", "epep");
    conn.openConnection();
    assertNotNull(conn);
  }

}
