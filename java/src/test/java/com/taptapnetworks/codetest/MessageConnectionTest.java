package com.taptapnetworks.codetest;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class MessageConnectionTest {

  @Before
  public void setUp() throws Exception {}

  @Test
  public void testHidePassword() {
    assertEquals("*********", MessageConnection.hidePassword("testpassw"));
  }
}
