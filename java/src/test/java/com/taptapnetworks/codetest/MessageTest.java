package com.taptapnetworks.codetest;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class MessageTest {

  Message msg = null;
  @Before
  public void setUp() throws Exception {
    msg = new Message("to", "message", "subject");
  }

  @Test
  public void testMessage() {
    assertEquals("to", msg.getTo());
    assertEquals("message", msg.getMessage());
    assertEquals("subject", msg.getSubject());
  }

  @Test
  public void testSetTo() {
    msg.setTo("otherTo");
    assertEquals("otherTo", msg.getTo());
  }


  @Test
  public void testSetMessage() {
    msg.setTo("otherMessage");
    assertEquals("otherMessage", msg.getMessage());
  }

  @Test
  public void testSetSubject() {
    msg.setTo("otherSubject");
    assertEquals("otherSubject", msg.getSubject());
  }

}
