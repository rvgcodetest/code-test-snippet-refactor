package com.taptapnetworks.codetest;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class MailConnectionTest {

  private MailConnection conn = null;
  
  @Before
  public void setUp() throws Exception {
    conn = new MailConnection("localhost");
  }

  @Test
  public void testOpenConnection() {
    conn.setCredentials("pepe", "epep");
    conn.openConnection();
    assertNotNull(conn);
  }

}
